#!/bin/bash
echo "Do you want to clean? (y/n)" && read cleanchoice
if [ $cleanchoice == "y" ]; then
  make clobber
  make clean
fi
export BUILD_WITH_COLORS=0
export ANDROID_COMPILE_WITH_BROTLI=false
. build/envsetup.sh
device="tissot"
devicedir="msm8953-common"
lunch revenge_$device-user
mka bacon -j$(nproc --all)
